server {
    listen *:80 default_server;

    server_name myproject.ll;
    root /project;


    location / {

       try_files $uri /index.php$is_args$args;

    }
    location ~* ^.*\.css$ {

        root /project/asset/css;
        rewrite "^(.*)/[\da-z]{5}/(.*)" $1/$2;

        expires 24h;
        add_header  Cache-Control "max-age=259200";
    }

    location ~* ^.*\.js$ {

        root /project/asset/js;
        rewrite "^(.*)/[\da-z]{5}/(.*)" $1/$2;

        expires 24h;
        add_header  Cache-Control "max-age=259200";
    }
    location ~* ^.*/(\d+)x(\d+)/(.+\.(?:jpg|gif|png))$ {
        proxy_cache       resized;
        proxy_cache_valid 180m;
        try_files "/project/app/upload;${uri}" @proxy_local;
        expires 24h;
    }
    location ~* ^.*/(.+\.(?:jpg|gif|png))$ {
            proxy_cache       resized;
            proxy_cache_valid 180m;
            try_files "/project/app/upload;${uri}" @proxy_local;
            expires 24h;
    }
    location ~ \.php$ {
        try_files $uri =404;
        limit_req zone=one burst=25;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

     location @proxy_local {
            proxy_temp_path /project/cust_images/data/temp;

            proxy_store    "/project/app/upload${uri}";
            proxy_store_access user:rw group:rw all:r;

            proxy_intercept_errors on;

            proxy_method            GET;
            proxy_pass_request_body        off;
            proxy_pass_request_headers    off;
            proxy_pass http://img.myproject.ll:9001;
     }

    error_log /var/log/nginx/api_error.log;
    access_log /var/log/nginx/api_access.log;
}

server {
    listen *:80;

    server_name admin.myproject.ll;
    root /project/admin/;

    location ~* ^.*\.css$ {

        root /project/asset/css;
        rewrite "^(.*)/[\da-z]{5}/(.*)" $1/$2;

        expires 24h;
        add_header  Cache-Control "max-age=259200";
    }

    location ~* ^.*\.js$ {

        root /project/asset/js;
        rewrite "^(.*)/[\da-z]{5}/(.*)" $1/$2;

        expires 24h;
        add_header  Cache-Control "max-age=259200";
    }
    location ~* ^.*/(\d+)x(\d+)/(.+\.(?:jpg|gif|png))$ {
         proxy_cache       resized;
         proxy_cache_valid 180m;
         try_files "/project/app/upload;${uri}" @proxy_local;
         expires 24h;
    }
    location ~* ^.*/(.+\.(?:jpg|gif|png))$ {
                proxy_cache       resized;
                proxy_cache_valid 180m;
                try_files "/project/app/upload;${uri}" @proxy_local;
                expires 24h;
    }

    location / {
            try_files $uri /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        limit_req zone=one burst=25;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location @proxy_local {
        proxy_temp_path /project/cust_images/data/temp;

        proxy_store    "/project/app/upload${uri}";
        proxy_store_access user:rw group:rw all:r;

        proxy_intercept_errors on;

        proxy_method            GET;
        proxy_pass_request_body        off;
        proxy_pass_request_headers    off;
        proxy_pass http://img.myproject.ll:9001;
    }

    error_log /var/log/nginx/api_error.log;
    access_log /var/log/nginx/api_access.log;
}

server {
	listen 9001;

	server_name img.myproject.ll

	error_log /var/log/nginx/api_error.log;
    access_log /var/log/nginx/api_access.log;

	expires 1d;

    location /upload {

        client_max_body_size 15g;

        root /project/app/upload;

        dav_access user:rw group:rw all:rw;

        dav_methods PUT DELETE MKCOL COPY MOVE;

        create_full_put_path on;

        autoindex on;
        autoindex_exact_size off;
        autoindex_localtime on;
        charset utf-8;

    }

    # регулярное выражение соответствует запросам к изображениям,
    # которые надо ресайзить, проверить можно по ссылке http://i1.phpschool.io:8080/150x150/php.png
    location ~ ^(.*)/(\d+)x(\d+)/(.+\.(?:jpg|gif|png))$ {
        alias /project/app/upload/$1$4;
        image_filter resize $2 $3;
        image_filter_buffer 16M;
    }

    location ~ / {
        root /project/app/upload;
    }

}