<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    protected $fillable = array("login",'pass');
    public $timestamps = false;


   /* public function roles()
    {
        return $this->belongsToMany('App\models\Roles');
    }*/

}