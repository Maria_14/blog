<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 12.11.18
 * Time: 12:50
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
use Rakit\Validation\Validator;
class Articles extends Model
{

    protected $table = "articles";
    protected $fillable = array('date_d', 'auth_id', 'title', 'description', 'topic_id', 'view', 'images');
    public $timestamps = false;


    public function rule()
    {
        return [
            'author_id'                  => 'required',
            'title'                 => 'required',
            'topic_id'                 => 'required',
            'description'           => 'required|min:6'
        ];
    }
    public function loadedElem(array $mas = [])
    {
        $this->author_id = $mas['author_id'];
        $this->date_d = date('Y-m-d');
        $this->title = $mas['title'];
        $this->description = $mas['description'];
        $this->topic_id = $mas['topic_id'];
        return true;
    }

    public function valid(array $mas = [])
    {
        $validator = new Validator;
        $validation = $validator->validate($mas, $this->rule());
        if ($validation->fails()) {
            $errors = $validation->errors()->toArray();
            return $errors;
        } else {
            return 'success';
        }

    }
    public function athor()
    {
        return $this->belongsTo('App\Models\Author','author_id');

    }
    public function tags()
    {
        return $this->belongsToMany('App\models\Tags', 'tag_article', 'id_article', 'id_tag');
    }

    public function topic()
    {
        return $this->belongsTo('App\Models\Topic','topic_id');
    }
    static public function getArticlesPage($page, $count)
    {
        $page = ($page -1) * $count;
        $article = DB::table('articles')
            ->offset($page)
            ->limit($count)
            ->get();
        return $article;
    }

}