<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "roles";
    protected $fillable = array('name');
    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany('App\models\User');
    }

}