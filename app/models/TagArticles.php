<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 13.11.18
 * Time: 20:53
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TagArticles extends Model
{
    protected $table = "tag_article";
    protected $fillable = array('id_article', 'id_tag');
    public $timestamps = false;
    public $idArticle;

    public function add($mas) {

        foreach ($mas as $kay => $value) {
            $model = new TagArticles();
            $model->id_article = $this->idArticle;
            $model->id_tag = $value;

            $model->save();

        }

    }
}