<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 10.11.18
 * Time: 16:17
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tokens;
use App\Models\User;

use Exception;
class Auth extends Model
{

    const SALT_LEN=10, SALT_POS=16;
    const COOKIE_NAME = "SESID";
    const SESSION_SALT_POS = 20;
    const SESSION_LIVE_TIME = 3600;

    public $error;

    private function generateSalt(){
        $t = sha1(time());
        $r = sha1(rand(10000,99999));
        $h = md5($t.$r);
        return substr($h,0,self::SALT_LEN);
    }

    private function repairSalt($hash){
        return substr($hash,self::SALT_POS,self::SALT_LEN);
    }

    private function testPass($pass,$hash){
        $hash2 = md5($pass);
        return $hash===$hash2;
    }



    private function createSessionKey($ip,$agent,$salt){
        $h1 = sha1(md5($ip).sha1($salt));
        $h2 = sha1(sha1($agent).md5($salt));
        $h3 = hash("sha256",$agent.$ip.$salt);
        $h = hash("sha256",$h2.$h3.$h1);
        return substr_replace($h,$salt,self::SESSION_SALT_POS,self::SALT_LEN);
    }

    private function repairSessionSalt($hash){
        return substr($hash,self::SESSION_SALT_POS,self::SALT_LEN);
    }

    private function testSesionKey($ip,$agent,$hash){
        $salt = $this->repairSessionSalt($hash);
        $hash2 = $this->createSessionKey($ip,$agent,$salt);
        return $hash===$hash2;
    }

    private function createSession($user){
        $ip = $_SERVER["REMOTE_ADDR"];
        $agent = md5($_SERVER["HTTP_USER_AGENT"]);
        $token = $this->createSessionKey($ip,$agent,$this->generateSalt());
        $exp = time()+self::SESSION_LIVE_TIME;

        $modelTokens = new Tokens();
        $modelTokens->users_id = (int)$user["id"];
        $modelTokens->user_ip = $ip;
        $modelTokens->agent = $agent;
        $modelTokens->token = $token;
        $modelTokens->expires = $exp;
        $modelTokens->save();

        setcookie(self::COOKIE_NAME,$token,$exp,URLROOT);
    }


    public function register($login,$pass,$data=[]) {
         $modelUser = new User();
        if(User::query()->where(['login' => $login])->count() > 0) {
            $this->error = "Username is used";
            return false;
        }
        $modelUser->login = $login;
        $modelUser->pass = md5($pass, PASSWORD_DEFAULT);
        $modelUser->save();
    }


    public function login($login,$pass){

        $user = User::query()->where(['login' => $login])->get()->toArray();

        if(empty($user[0])) {
            $this->error = "User not found";
            return false;
        }

        if(!$this->testPass($pass,$user[0]["pass"])) {
            $this->error = "Invalid password";
            return false;
        }
        $this->createSession($user);
    }

    public function isAuth() {

        if($this->auth!==NULL) return $this->auth;
        if(empty($_COOKIE[self::COOKIE_NAME])) return $this->auth=false;
        $ip = $_SERVER["REMOTE_ADDR"];
        $agent = md5($_SERVER["HTTP_USER_AGENT"]);

        if(!$this->testSesionKey($ip,$agent,$_COOKIE[self::COOKIE_NAME])) return $this->auth=false;
        $token = Tokens::query()->where(['user_ip'=>$ip, 'token'=>$_COOKIE[self::COOKIE_NAME], 'agent'=>$agent])->get()->toArray();

        if(!$token[0]) return $this->auth=false;


        if($token[0]["expires"]<time()){
            Tokens::query()->where(['token' => $token['id']])->delete();
            return $this->auth=false;
        }

        if($token[0]["expires"]-self::SESSION_LIVE_TIME/2<time()){

            Tokens::query()->where(['token' => $token['id']])->delete();

            $modelToken = Tokens::query()->where(['token' => $token['id']])->update(['expires' => self::SESSION_LIVE_TIME + time()]);

            setcookie(self::COOKIE_NAME,$_COOKIE[self::COOKIE_NAME],self::SESSION_LIVE_TIME + time(),URLROOT);
        }
        $user = User::query()->where(['id' => $token["users_id"]])->get()->toArray();

        $this->user = $user[0];

        return $this->auth=true;

    }
    public function getUser(){
        $token = Tokens::query()->where(['token' => $_COOKIE['SESID']])->get()->toArray();
        $user  = User::query()->where(['id' => $token['user_id']])->get()->toArray();

        return $user;
    }

    public function logout(){
        if(empty($_COOKIE[self::COOKIE_NAME])) return;
        Tokens::query()->where(['token' => $_COOKIE[self::COOKIE_NAME]])->delete();

    }
    public function logoutAll(){
        if( $this->isAuth())
            Tokens::query()->where(['user_id'=>$_COOKIE[self::COOKIE_NAME]])->delete();


    }






    public function hasRole($name)
    {
        if(!$this->isAuth()) return false;

        $users = User::with('roles')->get();
        var_dump($users);die();
      /*  if($this->roles===NULL){

            $r = QueryBuilder::instance()->roles->join("user_roles","id","roles_id")->getColumns(["name"],"`users_id`=?",[(int)($this->user["id"])]);

            $this->roles = array_column($r,"name");
        }

        return in_array($name,$this->roles);*/
    }




}