<div class="main form-style-8">
    <h2>Добавить статью</h2>
    <form action="" method="post" enctype="multipart/form-data">
        <select name="author_id">
            <option value = "1">Авторы</option>
            <?php foreach ($modelAuthor as $kay):?>
            <?php if($_POST['author_id'] == $kay['id']):?>
                <option  selected value = "<?= $kay['id']?>"><?= $kay['name']?></option>
            <?php else:?>
            <option   value = "<?= $kay['id']?>"><?= $kay['name']?></option>
            <?php endif;?>
            <?php endforeach;?>
        </select>

        <div class="error"><?=$error['auth_id']["required"]?></div>
        <input type="text" name="title" placeholder="Название статьи" value="<?=$_POST['title']?>"/>
        <div class="error"><?=$error['title']["required"]?></div>
        <select id="soflow" name="topic_id">
            <option value = "1">Тема статьи</option>
            <?php foreach ($modelTopic as $kay):?>
                <?php if($_POST['topic_id'] == $kay['id']):?>
                    <option  selected value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php else:?>
                    <option   value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php endif;?>
            <?php endforeach;?>
        </select>
        <div class="error"><?=$error['topic_id']["required"]?></div>
        <textarea placeholder="Описание" name = "description" value="<?=$_POST['description']?>" ></textarea>
        <div class="error"><?=$error['description']["required"]?></div>
        <div class="form-group">
            Теги к статье
            <p>
                <?php foreach ($modelTag as $kay):?>
                    <label>
                        <input type="checkbox" name="tags[]" value="<?= $kay['id']?>"/>
                        <span><?= $kay['name']?></span>
                    </label>
                <?php endforeach;?>
            </p>
        </div>
        <div class="error"><?=$error['tags']?></div>
        <div class="form-group">
            <span>Добавить изображение</span>
            <input type="file" name="images" id="images" value="<?=$_POST['images']?>">
            <div class="error"><?=$error['images']?></div>
        </div>
        <input type="submit" value="Добавить" />
    </form>
</div>
