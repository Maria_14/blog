<div class="main form-style-8">
    <section>

        <h1>Все статьи</h1>
        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
                <thead class="tbl-header">
                <tr>
                    <th>Дата создания</th>
                    <th>Название</th>
                    <th>Автор</th>
                    <th>Тема</th>
                    <th>Теги</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($article as $kay => $val): ?>
                <tr>
                    <td><?= date("d.m.Y", strtotime($val->date_d))?></td>
                    <td><?= $val->title?></td>
                    <td><?= $val->athor->name?></td>
                    <td><?= $val->topic->name?></td>
                    <td>
                    <?php foreach ($val->tags as $tags => $tag): ?>
                    <?= $tag->name?>
                    <br/>
                    <?php endforeach;?>
                    </td>
                    <td style="text-align: center"><a href="/article/update/<?= $val->id ?>">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="/article/delete/<?= $val->id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $links; ?>
            </div>
        </div>
    </section>
</div>