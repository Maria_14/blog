<div class="main form-style-8">
    <h2>Добавить статью</h2>
    <form action="" method="post" enctype="multipart/form-data">
        <select name="author_id">
            <option value = "1">Авторы</option>
            <?php foreach ($modelAuthor as $kay):?>
                <?php if($modelArticle['author_id'] == $kay['id']):?>
                    <option  selected value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php else:?>
                    <option value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php endif;?>
            <?php endforeach;?>
        </select>

        <div class="error"><?=$error['auth_id']["required"]?></div>
        <input type="text" name="title" placeholder="Название статьи" value="<?= htmlspecialchars($modelArticle['title'], ENT_QUOTES);?>"/>
        <div class="error"><?=$error['title']["required"]?></div>
        <select id="soflow" name="topic_id">
            <option value = "1">Тема статьи</option>
            <?php foreach ($modelTopic as $kay):?>
                <?php if($modelArticle['topic_id'] == $kay['id']):?>
                    <option  selected value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php else:?>
                    <option   value = "<?= $kay['id']?>"><?= $kay['name']?></option>
                <?php endif;?>
            <?php endforeach;?>
        </select>
        <div class="error"><?=$error['topic_id']["required"]?></div>
        <textarea placeholder="Описание" name = "description" value="<?=$modelArticle['description']?>" ><?=htmlspecialchars($modelArticle['description'], ENT_QUOTES);?></textarea>
        <div class="error"><?=$error['description']["required"]?></div>
        <div class="form-group">
            Теги к статье
            <p>
                <?php foreach ($modelTag as $kay) {
                    $flag = 0;
                    for ($i = 0; $i < count($tags); $i++) {
                        if ($kay['id'] == $tags[$i]) {
                            $flag = 1;?>
                            <label>
                                <input checked type="checkbox" name="tags[]" value="<?= $kay['id']?>"/>
                                <span><?= $kay['name']?></span>
                            </label>
                           <?php break;
                        }
                    }
                    if($flag==1) continue;?>
                    <label>
                        <input type="checkbox" name="tags[]" value="<?= $kay['id']?>"/>
                        <span><?= $kay['name']?></span>
                    </label>
              <?php  }
                ?>
            </p>
        </div>
        <div class="error"><?=$error['tags']?></div>
        <div class="form-group">
            <div><img src="/150x150/<?= $modelArticle['images']?>"></div>
            <span>Изменить изображение</span>
            <input type="file" name="images" id="images" value="/<?=$modelArticle['images']?>"  accept=".jpg, .jpeg, .png">
            <div class="error"><?=$error['images']?></div>
        </div>
        <input type="submit" value="Изменить" />
    </form>
</div>
