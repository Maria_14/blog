<div id="banner"></div>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="left">
                <section>
                    <header>
                        <h2>Статьи</h2>
                    </header>
                        <ul type="1" >
                        <?php foreach ($article as $kay => $val):?>
                            <li><a class="link" href="/post/<?=$val->id?>"><?=$val->title?></a></li>
                        <?php endforeach;?>
                        </ul>
                    <div class="pagination">
                        <?php echo $links; ?>
                    </div>
               </section>
            </div>
            <div class="right">
                <section class="sidebar">
                    <header>
                        <h2>Облако тегов по темам статей</h2>
                        <img id = "loader1" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                    </header>
                    <ul class="default-topic default"></ul>
                </section>
                <section class="sidebar">
                    <header>
                        <h2>Облако тегов по авторам</h2>
                        <img id = "loader2" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                    </header>
                    <ul class="default-author default"></ul>
                </section>
                <section class="sidebar">
                    <header>
                        <h2>Облако тегов по дате публикации</h2>
                        <img id = "loader3" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                    </header>
                    <ul class="default-date default"></ul>
                </section>
            </div>
        </div>
    </div>
</div>