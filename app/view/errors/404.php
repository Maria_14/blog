<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>404</title>

    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

    <link href="/12345/error.css" rel="stylesheet">

</head>
<body>
<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h3>Такой страницы нет</h3>
            <h1><span>4</span><span>0</span><span>4</span></h1>
        </div>
        <h2>Нам жаль, что ее нет...</h2>
    </div>
</div>
</body>
</html>
