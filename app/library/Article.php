<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 23.11.18
 * Time: 19:24
 */

namespace App\Library;


class Article extends Base
{
    /**
     * Default host.
     *
     * @var string
     */
    public $host = null;
    /**
     * Default port / channel.
     *
     * @var int
     */
    public $port = null;

    /**
     * Initialize the client connection
     * given the host, port,
     *
     * @param   string
     * @param   int
     */
    public function __construct($host, $port = 80)
    {
        // set the host
        $this->host     = $host;
        // set the port
        $this->port     = $port;
    }
    /**
     * Set host.
     *
     * @param   string
     * @return  $this
     */
    public function setHost($host)
    {
        // set the host
        $this->host = $host;
        return $this;
    }
    /**
     * Set port.
     *
     * @param   int
     * @return  $this
     */
    public function setPort($port = 80)
    {
        // set the port / channel
        $this->port = $port;
        return $this;
    }

}