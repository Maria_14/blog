<?php
/**
* 
*/
namespace App\Library;
class Helper 
{
    
    function __construct()
    {
        # code...
    }

     public  static  function pagination ($active, $count, $base_url){
        $res="<ul>";
        if($active!=1) $res.="<li><a href =".$base_url."1>&lt;</a></li>";
        if($active>2) $res.="<li><a href=".$base_url.($active-2).">".($active-2)."</a></li>";
        if($active>1) $res.="<li><a href=".$base_url.($active-1).">".($active-1)."</a></li>";
        $res.="<li><a class = active>".$active."</a></li>";
        if($active<$count) $res.="<li><a href=".$base_url.($active+1).">".($active+1)."</a></li>";
        if($active<$count-1) $res.="<li><a href=".$base_url.($active+2).">".($active+2)."</a></li>";
        if($active<$count) $res.="<li><a href=".$base_url.($count).">&gt;</a></li>";
        $res.="</ul>";
        return $res;

    }
    public static function JS($name)
    {
        return "<script type='text/javascript' src='/12345/{$name}.js'></script>";
    }

}
   