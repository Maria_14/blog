<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 20.11.18
 * Time: 22:35
 */

namespace App\Library;

use Faker\Generator;
use App\Library\SqlQuery;
use Illuminate\Database\Capsule\Manager as DB;
use App\Models\Author;
use App\models\Topic;
use App\models\Articles;
use App\Core\Classes\Database;
use Faker;


class FakeDump
{
    public $data = [];
    public $arrayKay = [];
    public $arrayValue = [];
    /*
     * path where save dump
     * */
    public $file;

    public $arrayLocal = [
        'ar_JO', 'ar_SA','bg_BG','bn_BD', 'cs_CZ', 'da_DK',
        'de_CH', 'en_IN','en_NG', 'en_UG', 'en_US', 'en_ZA',
        'es_AR', 'es_ES', 'es_PE', 'es_VE', 'fa_IR', 'fi_FI',
        'fr_BE','fr_CA', 'hr_HR','kk_KZ', 'ko_KR', 'it_LT',
        'lv_LV', 'me_ME', 'mn_MN', 'ms_MY', 'nb_NO', 'ne_NP',
        'nl_BE', 'nl_NL', 'pl_PL', 'pt_BR', 'pt_PT', 'ro_MD',
        'ro_RO', 'ru_RU', 'sk_SK', 'sl_SL', 'sr_Cyrl_RS', 'ar_RS',
        'sv_SE', 'th_TH', 'tr_TR', 'uk_UA', 'vi_VN', 'zh_CN', 'zh_TW',
        'de_AT','de_DE','el_CY'
    ];

    public function dumpAuthor()
    {
        $k = 0;
        $this->arrayKay = ['name'];

        do {
            $faker = Faker\Factory::create($this->arrayLocal[$k]);
            try {
                for ($j=0; $j < 89; $j++) {
                    array_push($this->arrayValue, $faker->unique()->name);
                }
            } catch (\OverflowException $e) {
                $k++;
                continue;
            }
            $k++;
        } while ($k<count($this->arrayLocal));

        $array = [];
        for ($i = 1; $i < count($this->arrayValue) + 1; $i++) {
            if(($i%1000) == 0) {
                $this->saveStringPackeg($this->arrayKay, $array, 'author');
                $array = [];
            }
            array_push($array,  [$this->arrayValue[$i]]);
        }

    }

    public function saveStringPackeg($arrayKay, $arrayValue, $tabel)
    {
        $csv = new SqlQuery();
        $handle = fopen($this->file, "a");
        $value = $csv->getStringInsertPackeg($arrayKay, $arrayValue, $tabel);

        fputcsv($handle, explode(";", $value), ";", ' ');
        fclose($handle);

    }

    public function saveString($arrayKay, $tabel)
    {
        $csv = new SqlQuery();
        $handle = fopen($this->file, "a");
        $value = $csv->getStringInsert($arrayKay, $tabel);

        fputcsv($handle, explode(";", $value), ";", ' ');
        fclose($handle);
    }
    public function dumpTopic()
    {
        $faker = Faker\Factory::create('en_IN');
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $this->arrayKay = [];
        for ($i = 0; $i < 30; $i++) {
            try {
                $this->arrayKay['name'] = $faker->word;
                $this->saveString($this->arrayKay, 'topic');
            } catch (\OverflowException $e) {
                continue;
            }
        }

    }
    public function dumpArticles()
    {
        $faker = Faker\Factory::create('en_IN');
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\Title($faker));
        $faker->addProvider(new Faker\Provider\Image($faker));
        $masImg = scandir('/project/app/upload/');
        $masImgCount = count($masImg);
        $array = [];
        $this->arrayKay = ['date_d','author_id', 'title', 'description', 'topic_id', 'view', 'images'];
        $this->arrayValue = [];
        for ($i = 1; $i < 500000; $i++) {
            try{
                if(($i%10000) == 0) {
                    $this->saveStringPackeg($this->arrayKay, $array, 'articles');
                    $array = [];
                }
                $dateTime = $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now');
                foreach ($dateTime as $val => $kay)
                {
                    if($val == "date"){
                        $date = explode(" ", $kay);
                        $this->arrayValue[] = $date[0];
                        break;
                    }
                }
                $this->arrayValue[] = $faker->numberBetween('1', '5000');
                $this->arrayValue[] = $faker->title('3');
                $this->arrayValue[] = $faker->realText();
                $this->arrayValue[] = $faker->numberBetween('1', '30');
                $this->arrayValue[] = $faker->numberBetween('1', '3');
                $random = $faker->numberBetween('3', $masImgCount);
                $this->arrayValue[] = $masImg[$random];
                array_push($array,  [$this->arrayValue]);
                $this->arrayValue = [];
            } catch (\OverflowException $e) {
                continue;
            }
        }
    }
    public function dumpTagArticle()
    {
        $faker = Faker\Factory::create('ru_RU');
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $this->arrayKay = ['id_article','id_tag'];
        $this->arrayValue = [];
        $array = [];
        for ($i = 1; $i < 1000000; $i++) {
            if(($i%10000) == 0) {
               $this->saveStringPackeg($this->arrayKay, $array, 'tag_article');

                $array = [];
            }
            $this->arrayValue[] = $faker->numberBetween('1', '500000');
            $this->arrayValue[] = $faker->numberBetween('1', '50');
            array_push($array,  [$this->arrayValue]);

            $this->arrayValue = [];
        }
    }
    public function dumpTag()
    {
        $faker = Faker\Factory::create('ru_RU');
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $this->arrayKay = ['name'];
        $this->arrayValue = [];
        $array = [];
        for ($i = 1; $i < 51; $i++) {
            if(($i%50) == 0) {
              $this->saveStringPackeg($this->arrayKay, $array, 'tag');
                $array = [];
            }
            $this->arrayValue[] = $faker->word;
            array_push($array,  [$this->arrayValue]);
            $this->arrayValue = [];
        }
    }

}