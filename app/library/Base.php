<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 23.11.18
 * Time: 19:25
 */

namespace App\Library;


class Base
{

    public $article;
    /**
     * Initialize the client connection
     * given the host, port
     *
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }
    /**
     * Does a curl in the article.myproject.ll
     *
     * @param string $route
     * @param array $params
     * @return resource
     */
    public function connect( $route, array $params = [], array $data = [])
    {
        $url  = "http://" . $this->article->host;
        $url .= $route . '?' . http_build_query($params);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_PORT, $this->article->port);
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, count($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        $results = curl_exec($curl);
        curl_close($curl);

        return $results;
    }
}