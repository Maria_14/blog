<?php
 /**
 *
 */
namespace App\Library;

use PDO;
use App\Core\Classes\Config;



 class Db
 {
 	private $DBH;

 	private static $instance;

 	public static function getConnect(){
        if(self::$instance === null){
            $config = Config::Load('database');

           self::$instance = new PDO(
            "mysql:host={$config["host"]};port={$config["port"]};dbname={$config["dbname"]};charset={$config["charset"]}",
            $config["user"],
            $config["pass"],
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,

            ]
        );
           return self::$instance;

       }else{

            return self::$instance;
       }

 	}

 	 private  function __construct(){}
     private function __clone(){}



 }