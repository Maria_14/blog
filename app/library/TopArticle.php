<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 23.11.18
 * Time: 19:50
 */

namespace App\Library;


class TopArticle extends Base
{
    /**
     * @const string update route URL.
     */
    const UPDATE_ROUTE = '/update';
    /**
     * @const string fix-count route URL.
     */
    const FIX_COUNT_ROUTE = '/fix-count';

    /**
     * Update Article
     *
     * @param int $id
     * @param int $id_topic
     * @return array
     */
    public function updateArticle($id, $id_topic)
    {
        $data = array(
            'id' => $id,
            'id_topic' => $id_topic
        );
        $response = $this->connect(self::UPDATE_ROUTE, [], $data);

        return $response;
    }
    /**
     * Fix Count Article
     *
     * @param int $id
     * @param int $id_topic
     * @param mixed $ip
     * @return array
     */
    public function fixCount($id, $id_topic, $ip = null)
    {
        $data = array(
            'id' => $id,
            'id_topic' => $id_topic,
            'ip' => $ip
        );
        $response = $this->connect(self::UPDATE_ROUTE, [], $data);

        return $response;
    }

}