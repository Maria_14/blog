<?php
/**
* 
*/
namespace App\Core\Classes;

use App\Core\Classes\View;

abstract class Controller 
{
	public $route;
	public $view;
	
	public function __construct($route)
	{
        $this->behavior();
		$this->route = $route;
		$this->view = new View($route);


	}
    public function redirect($url){
        $this->header("Location",$url);
    }

    public function header($name,$value){
        header("{$name}:{$value}");
    }

    protected function behavior(){}


}