<?php
/**
* 
*/

namespace App\Core\Classes;
use App\Core\Classes\Config;


class Router
{


	protected $routes = [];
	public $params = [];
	public function __construct($routs){

		$arr = Config::Load($routs);

		foreach ($arr as $key => $val) {
		 	$this->add($key, $val);
		 } 

	}

	public function add($route, $params)
	{
		$route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $route);
		$route = '#^'.$route.'$#';
		$this->routes[$route] = $params;

	}
	public function match()
	{
		 $url = trim($_SERVER['REQUEST_URI'], '/');

		 foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {

                foreach ($matches as $key => $match) {

                	$match = $_GET;
                    $params[$key] = $match;
                    $params['matches'] = $matches;
                    
                }
                $this->params = $params;

                return true;
            }
        }
		 return false;
	}
	public function run()
	{
		if($this->match()){
			$path = 'App\\Controllers\\'.ucfirst($this->params['controller']);

			if(class_exists($path)){
				$action = $this->params['action'].'Action';
				if(method_exists($path, $action)){
					$controller = new $path($this->params);
                    $controller->$action();
				}
				
			}else{
                View::errorCode(404);
			}
		}else{
            View::errorCode(404);
		}
		
	}

}