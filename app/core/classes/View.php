<?php

/**
* 
*/
namespace App\Core\Classes;
use App\Library\Helper;

class View 
{
	public $path;
	public $route;
	public $layout = 'default';

	public function __construct($route){
		$this->route = $route;
		$this->path = $route['controller'].'/'.$route['action'];
		
	

	}
    public function addJs($name){
	    if($name){
            $this->js[] = Helper::JS($name);
        }

    }

    public function __set($name, $value){
        $this->data[$name] = $value;
    }
	public function render($data=[]) {
		
		$path = __ROOT__.'/view/'.$this->path.'.php';
		extract($data);
		if (file_exists($path)) {
			$content =  $path;
			if($this->js) {
                $js = implode("\r\n",$this->js);
            }

			require __ROOT__.'/view/layouts/'.$this->layout.'.php';
		}
	}


    public static function errorCode($code) {
        http_response_code($code);
        $path = __ROOT__.'/view/errors/'.$code.'.php';
        if (file_exists($path)) {
            require $path;
        }
        exit;
    }

}