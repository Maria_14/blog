<?php
return [
    '' => [
        'controller' => 'admin',
        'action' => 'admin',
    ],
    '/' => [
        'controller' => 'admin',
        'action' => 'admin',
    ],
    'admin' => [
        'controller' => 'admin',
        'action' => 'admin',
    ],
    'dump' => [
        'controller' => 'admin',
        'action' => 'dump',
    ],
    'login{post:(.*?)}' => [
        'controller' => 'admin',
        'action' => 'login',
    ],
    'add' => [
        'controller' => 'admin',
        'action' => 'add',
    ],
    'logout' => [
        'controller' => 'admin',
        'action' => 'logout',
    ],
    'article/create' => [
        'controller' => 'article',
        'action' => 'create',
    ],
    'article/update/{id:\d+}' => [
        'controller' => 'article',
        'action' => 'update',
    ],
    'article/delete/{id:\d+}' => [
        'controller' => 'article',
        'action' => 'delete',
    ],
    'article{page:(.*?)}' => [
        'controller' => 'article',
        'action' => 'index',
    ],
    'article/{page:(.*?)}' => [
        'controller' => 'article',
        'action' => 'index',
    ],



];