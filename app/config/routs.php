<?php
return [
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],
    'index{page:(.*?)}' => [
        'controller' => 'main',
        'action' => 'index',
    ],
    'main{page:(.*?)}' => [
        'controller' => 'main',
        'action' => 'index',
    ],

	'post/{id:\d+}' => [
	    'controller' => 'blog',
		'action'=> 'post',
	],
    'getcloudcount' => [
        'controller' => 'main',
        'action' => 'getcloudcount',
    ],
    'getcloudauthor' => [
        'controller' => 'main',
        'action' => 'getcloudauthor',
    ],
    'getclouddate' => [
        'controller' => 'main',
        'action' => 'getclouddate',
    ],
    'toppost' => [
        'controller' => 'blog',
        'action'=> 'toppost'
    ]

];
?>