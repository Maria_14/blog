<?php
/**
* 
*/
namespace App\Controllers;


use App\Core\Classes\Controller;
use Illuminate\Database\Capsule\Manager as DB;
use App\Models\Articles;
use App\Library\Helper;



class Main  extends Controller
{
	
	public function indexAction()
	{
        $page = $_GET['page'];
        $page = $page?$page:1;
        $countPage = 30;
        if($page == 1){
            $mas = $this->createArray($page, $countPage);
        } else {
            $lastParam = $page * $countPage;
            $firstParam = $lastParam - $countPage - 1;
            $mas = $this->createArray($firstParam, $lastParam);
        }
        $items = Articles::find($mas);
        $pagination = Helper::pagination($page, ceil(Articles::query()->get()->count()/$countPage), "main?page=");


        $this->view->addJs('cloud');
        $this->view->render([
            'article'=>$items,
            'links'=> $pagination]);
	}
	public function getcloudcountAction()
    {

      $tags = DB::table('tag')
            ->leftjoin('tag_article', 'tag.id', '=', 'tag_article.id_tag')
            ->leftjoin('articles', 'articles.id', '=', 'tag_article.id_article')
            ->selectRaw(' count(articles.id) AS `count`, GROUP_CONCAT(tag.name) AS `tag`');
        $cloudTopic = $tags->groupBy('topic_id')->orderBy('count','desc')->limit(1)->get();
        $mas = [
            'cloudTopic' => $cloudTopic,
        ];
        echo json_encode($mas);
    }
    public function getcloudauthorAction()
    {
        $tags = DB::table('articles')
            ->leftjoin('tag_article', 'articles.id', '=', 'tag_article.id_article')
            ->leftjoin('tag', 'tag.id', '=', 'tag_article.id_tag')
            ->selectRaw(' GROUP_CONCAT(tag.name) AS `tag`')
            ->join('author', 'author.id', '=', 'articles.author_id');
        $cloudAuthor = $tags->groupBy('author_id')->orderBy('author.name', 'asc')->limit(1)->get();
        $mas = [
            'cloudAuthor' => $cloudAuthor,
        ];
        echo json_encode($mas);
    }
    public function getclouddateAction()
    {
        $tags = DB::table('articles')
            ->leftjoin('tag_article', 'articles.id', '=', 'tag_article.id_article')
            ->leftjoin('tag', 'tag.id', '=', 'tag_article.id_tag')
            ->selectRaw(' GROUP_CONCAT(tag.name) AS `tag`');
        $cloudDate = $tags->groupBy('articles.date_d')->orderBy('articles.date_d')->limit(1)->get();
        $mas = [
            'cloudDate' => $cloudDate
        ];
        echo json_encode($mas);
    }


    public function createArray($firsParam, $lastParam)
    {
        $mas = [];
        for ($i=$firsParam; $i<=$lastParam; $i++){
            array_push($mas, $i);
        }
        return $mas;

    }

}