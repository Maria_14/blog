<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 13.11.18
 * Time: 8:17
 */

namespace App\Controllers;

use App\Core\Classes\Controller;
use App\Models\Auth;
use App\Models\Author;
use App\Library\Upload;
use App\Models\Articles;
use App\Models\TagArticles;
use App\Models\Topic;
use App\Models\Tags;
use App\Library\Helper;
use App\Library\TopPush;
class Article extends Controller {


   public function behavior()
   {
       $modelAuth = new Auth();

       if (!$modelAuth->isAuth() && !$modelAuth->hasRole("admin")) {
           $this->redirect(("/login"));
       }

   }
   public function indexAction()
   {
       $this->view->layout = "admin";
       $page = $_GET['page'];
       $page = $page?$page:1;
       $countPage = 15;
       if($page == 1){
          $mas = $this->createArray(1, 15);
       } else {
           $lastParam = $page * $countPage;
           $firstParam = $lastParam - $countPage - 1;
           $mas = $this->createArray($firstParam, $lastParam);
       }
       $articles = Articles::find($mas);
       $pagination = Helper::pagination($page, ceil(Articles::query()->get()->count()/$countPage), "?page=");
       $this->view->render([
           'article'=>$articles,
           'links'=> $pagination]);

   }

    public function createAction()
    {

        $this->view->layout = "admin";
        $error = [];
        if(!empty($_FILES['images']['tmp_name'])) {
            $modelUpload = new Upload();
            $path = $modelUpload->upload('images', $_FILES, null);
            if($path['error']) {
                $error['images'] = $path['error'];

            }
        }
        if($_POST && !$path['error']) {
            $model = new Articles();
            $model->path = $path['path'];

            if($model->loadedElem($_POST) && $response = $model->valid($_POST) == 'success' && $model->save()){
                if($_POST['tags']) {
                    $modelTag = new TagArticles();
                    $modelTag->idArticle =  $model->id;
                    $modelTag->add($_POST['tags']);
                    $this->redirect(("create"));
                }
            } else {
                foreach ($response as $kay => $value) {
                    $error[$kay] = $value;
                }
            }
        }
        $modelTopic = Topic::all()->toArray();
        $modelTag = Tags::all()->toArray();
        $modelAuthor = Author::all()->toArray();
        $this->view->render([
            'error'=>$error,
            'modelTopic' => $modelTopic,
            'modelTag'=>$modelTag,
            'modelAuthor'=>$modelAuthor]);
    }
    public function updateAction()
    {


        $this->view->layout = "admin";
        $id = $this->route['matches']['id'];

        if(!$id) $this->view->errorCode(404);
        $modelArticle = Articles::find($id);
        if(!$modelArticle) $this->view->errorCode(404);
        $modelTag = $modelArticle->get();
        $topicId = $modelArticle->topic_id;

        $modelArticle->view = 0;
        $modelArticle->save();
        $data['id'] = $id;
        $data['topic_id'] = $topicId;
        $data['rout'] = 'update';

        $topPush = new TopPush();
        $topPush->connect($data);


        if(!empty($_FILES['images']['tmp_name'])) {
            $modelUpload = new Upload();
            $modelUpload->find(__ROOT__."/upload/",$modelArticle->toArray()['images']);
            $path = $modelUpload->upload('images', $_FILES, null);

            if($path['error']) {
                $error['images'] = $path['error'];
            } else {
                $modelArticle->images  = $path['path'];
            }
        }

        if($_POST && !$path['error']) {
            if($modelArticle->loadedElem($_POST) && $response = $modelArticle->valid($_POST) == 'success' && $modelArticle->save()){
                $modelArticle->tags()->detach();

                if($_POST['tags']) {
                    $modelTag = new TagArticles();
                    $modelTag->idArticle = $id;


                    $modelTag->add($_POST['tags']);
                    $this->redirect(("/article/"));
                }
            } else {
                foreach ($response as $kay => $value) {
                    $error[$kay] = $value;
                }
            }
        }

        $tags = [];
        foreach ($modelArticle->tags as $kay => $val)
        {
            $tags[] = $val['id'];
        }
        $modelArticle = $modelArticle->toArray();
        $modelTopic = Topic::all()->toArray();
        $modelAuthor = Author::all()->toArray();
        $modelTag = Tags::all()->toArray();

        $this->view->render([
            'error'=>$error,
            'modelArticle'=>$modelArticle,
            'modelTopic' => $modelTopic,
            'modelTag'=>$modelTag,
            'tags'=>$tags,
            'modelAuthor'=>$modelAuthor
        ]);
    }
    public function deleteAction()
    {
        $id = $this->route['matches']['id'];
        if(!$id) $this->view->errorCode(404);
        $modelArticle = Articles::find($id);
        if(!$modelArticle) $this->view->errorCode(404);
        $modelArticle->tags()->detach();
        $modelUpload = new Upload();
        $modelUpload->find(__ROOT__."/upload/",$modelArticle->toArray()['images']);
        $modelArticle->delete();
        $this->redirect(("index"));
    }

    public function createArray($firsParam, $lastParam)
    {
        $mas = [];
        for ($i=$firsParam; $i<=$lastParam; $i++){
             array_push($mas, $i);
        }
        return $mas;

    }

}