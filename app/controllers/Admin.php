<?php
/**
* 
*/
namespace App\controllers;

use App\Core\Classes\Controller;
use App\Models\Auth;
use App\Models\Author;
use App\Models\Articles;
use App\Library\Upload;
use App\models\Topic;
use Rakit\Validation\Validator;
use Faker\Generator;
use Faker;
use App\Library\SqlQuery;
use Illuminate\Database\Capsule\Manager as DB;





class Admin  extends Controller {


	public function adminAction()
	{
        $this->view->layout = "admin";

        $modelAuth = new Auth();

        if (!$modelAuth->isAuth() && !$modelAuth->hasRole("admin")) {
            $this->redirect(("login"));
        }

		$this->view->render();
		
	}
    public function loginAction()
    {

        $this->view->layout = "login";
        $modelAuth = new Auth;
        $password = "";
        $username ="";

        if($_POST) {
            $validator = new Validator;

            $validation = $validator->validate($_POST, [
                'username' => 'required',
                'password' => 'required',
            ]);
            $validation->setAliases([
                'username' => 'Error username',
                'password' => 'Error in your password'
            ]);

            if(!empty($validation->errors()->get('username'))) {
                $username = $validation->getAlias('username');
            }

            if(!empty($validation->errors()->get('password'))) {
                $password = $validation->getAlias('password');
            }

            if($validation->errors()->count() < 1) {
                $modelAuth->login($_POST["username"], $_POST["password"]);
                $this->redirect(("admin"));
            }

        }
        $this->view->render([
            'errorPass' => $password,
            'errorUserName' => $username
        ]);

    }

	public function logoutAction()
    {
        $modelAuth = new Auth;
        $modelAuth->logout();
        $this->redirect(("admin"));

    }
}