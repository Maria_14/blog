<?php
/**
* 
*/
namespace App\Controllers;

use App\Core\Classes\Controller;
use App\Models\Articles;
use Illuminate\Database\Capsule\Manager as DB;
use App\Library\TopPush;

class Blog  extends Controller{

    public function postAction()
    {
        $id = $this->route['matches']['id'];
        $modelArticle = Articles::find($id);
        $auth = $modelArticle->athor->name;
        $topic = $modelArticle->topic->name;
        $topicId = $modelArticle->topic_id;
        $topArticle = $users = DB::table('articles')
            ->select(['id', 'view', 'title', 'images'])
            ->where(['topic_id' => $topicId])
            ->where('id', '<>', $id)
            ->orderBy('view', 'DESC')
            ->limit(3)
            ->get();
        $modelArticle->view = $modelArticle->view + 1;
        $modelArticle->save();
        $data['id'] = $id;
        $data['topic_id'] = $topicId;
        $data['rout'] = 'fixcount';

        $topPush = new TopPush();
        $topPush->connect($data);

        $this->view->addJs('main');
        $this->view->render([
            'modelArticle'=>$modelArticle->toArray(),
            'auth'=>$auth,
            'topic' => $topic,
            'topArticle' => $topArticle
        ]);
    }
    public function toppostAction()
    {

        if(empty($_POST['article_id'])) {
            echo json_encode(['error'=>'Bad Request']);
        }
        $modelArticle = Articles::where('id', '=', $_POST['article_id'])->first();
        $modelArticle->view = $_POST['count_view'];
        $modelArticle->save();
        $mas = [];
      foreach($_POST[0] as $kay => $value){

          $topArticle = $users = DB::table('articles')
              ->select(['id', 'view', 'title', 'images'])
              ->where('id', '=', $value['article_id'])
              ->get();
          array_push($mas, $topArticle);
       }

        echo json_encode($mas);
    }
}