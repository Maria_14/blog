var page ={
    init:function () {

        var self = this;
        var ContainerTopArticle = $(".container.top-article");
        Pusher.logToConsole = true;

        var pusher = new Pusher('4f4af9d0ae2fe5c08c28', {
            cluster: 'eu',
            //forceTLS: true
        });
        var channel = pusher.subscribe('my-channel');

        channel.bind('my-event', function(data) {
            $(".topic.view span").html(data.count_view);
            var url = "/toppost";
            self.ajaxCall(url, data, function(msg) {
                ContainerTopArticle.empty();
                $.each(msg, function (kay, value) {
                    $.each (value, function (kay, value) {
                        let block =  self.create(value.images, "/post/" + value.id, value.view, value.title);

                       block.appendTo(ContainerTopArticle);
                    })
                });
            });
        });

    },
    create:function(url, href, views, titles){
        let mainBlock =  jQuery('<div/>', {
            class:"col-article"
        });
       let images =  jQuery('<div/>', {
            style:"width:100%; height:100px; background-image: url(/"+url+");background-position: 50% 50%"
        });
        let link = jQuery('<a/>',{
           href:href
       });
        images.appendTo(link);
        link.appendTo(mainBlock);
        let view = jQuery('<p/>', {
            class: 'small',
            text:"Просмотры:" + views
        });
        let title = jQuery('<p/>', {
            text: titles
        });
        title.appendTo(link);
        let blockTitle =  jQuery('<div/>', {
            class: 'top-article-title',
        });
        title.appendTo(blockTitle);
        view.appendTo(blockTitle);


        blockTitle.appendTo(mainBlock);
         return mainBlock;
    },
    ajaxCall:function (ajax_url, ajax_data, successCallback) {
        $.ajax({
            type : "POST",
            url : ajax_url,
            dataType : "json",
            data: ajax_data,
            success : function(msg) {
                if(msg.error) {
                    console.log(msg);
                } else {
                    successCallback(msg);
                }
            },
            error: function(msg) {
                console.log(msg);
            }
        });

    }
};



window.addEventListener("load", function () {
    page.init();
});


