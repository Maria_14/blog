var cloud = {
    init: function () {
        var url1 = "/getcloudcount";
        var url2 = "/getcloudauthor";
        var url3 = "/getclouddate";
        var loader1  = $("#loader1");
        var loader2  = $("#loader2");
        var loader3  = $("#loader3");
        var self= this;
        var defaultTopic = $(".default-topic");
        var defaultAuthor = $(".default-author");
        var defaultDate = $(".default-date");
        var tag;
        this.ajaxCall(url1, [], function(msg) {
                $.each(msg.cloudTopic, function (kay, value) {
                    tag = (value.tag).split(',');
                    self.generatorCloud(tag, defaultTopic);
                    tag = [];
                })
        },loader1);
       this.ajaxCall(url2, [], function(msg) {
           $.each(msg.cloudAuthor, function (kay, value) {
               tag = (value.tag).split(',');
               self.generatorCloud(tag, defaultAuthor);
               tag = [];
           })
        },loader2);
        this.ajaxCall(url3, [], function(msg) {
            $.each(msg.cloudDate, function (kay, value) {
                tag = (value.tag).split(',');
                self.generatorCloud(tag, defaultDate);
                tag = [];
            })
        }, loader3);

    },
    ajaxCall:function (ajax_url, ajax_data, successCallback, loader) {
        $.ajax({
            type : "GET",
            url : ajax_url,
            dataType : "json",
            data: ajax_data,
            success : function(msg) {
                if(msg.error) {
                    console.log(msg);
                } else {
                    successCallback(msg);
                }
            },
            error: function(msg) {
                console.log(msg);
            },
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            }
        });

    },
    generatorCloud: function (array, elem) {
        var li = jQuery('<li/>');
        var link = "";
        $.each(array, function (kay, value) {
            link = jQuery('<a/>',{
                href:'#',
                text:value
            });
            link.appendTo(li);
            li.appendTo(elem);
        });

    }
};


window.addEventListener("load", function () {
    cloud.init();
});